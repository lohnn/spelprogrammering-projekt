﻿using UnityEngine;
using System.Collections;

namespace Math
{
	[System.Serializable]
	public class IntVector2
	{
		public enum Direction
		{
			North,
			East,
			South,
			West
		}
	
		public int x;
		public int y;

		public IntVector2 (int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public IntVector2 (IntVector2 intVector)
			:this(intVector.x, intVector.y)
		{
		}

		public void Move (Direction direction)
		{
			switch (direction) {
				case Direction.North:
					y--;
					break;
				case Direction.South:
					y++;
					break;
				case Direction.East:
					x++;
					break;
				case Direction.West:
					x--;
					break;
			}
		}
		
		public override bool Equals (object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}
			
			// If parameter cannot be cast to Point return false.
			IntVector2 p = obj as IntVector2;
			if ((System.Object)p == null)
			{
				return false;
			}
			
			// Return true if the fields match:
			return (x == p.x) && (y == p.y);
		}
		
		public bool Equals(IntVector2 p)
		{
			// If parameter is null return false:
			if ((object)p == null)
			{
				return false;
			}
			
			// Return true if the fields match:
			return (x == p.x) && (y == p.y);
		}
		
		public override int GetHashCode()
		{
			return x ^ y;
		}
		
		public static bool operator ==(IntVector2 a, IntVector2 b)
		{
			// If both are null, or both are same instance, return true.
			if (System.Object.ReferenceEquals(a, b))
			{
				return true;
			}
			
			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null))
			{
				return false;
			}
			
			// Return true if the fields match:
			return a.x == b.x && a.y == b.y;
		}
		
		public static bool operator !=(IntVector2 a, IntVector2 b)
		{
			return !(a == b);
		}

		public override string ToString ()
		{
			return string.Format ("({0}, {1})", x, y);
		}
	}
}
