﻿using UnityEngine;
using System.Collections;

namespace Model.Map
{
	public abstract class SpecialPartModel : MonoBehaviour
	{
		[SerializeField]
		public GameObject
			Model = null;

		public abstract void Collide (BallModel ball);

		public abstract void Reset ();
	}
}
