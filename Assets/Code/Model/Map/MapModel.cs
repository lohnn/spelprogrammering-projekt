using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Math;

namespace Model.Map
{
	public class MapModel : MonoBehaviour
	{
		public enum MapState
		{
			None,
			Won,
			Lost
		}
		[SerializeField]
		private Row[]
			mapPieces = null;
		[SerializeField]
		public SpecialPartModel[]
			SpecialParts = null;
		public BallModel
			ball;
		public IntVector2
			ballStartPos;
		public IntVector2.Direction
			ballStartDir;
		public MapState mapState = MapState.None;
		//public bool hasWon = false;
		
		public IntVector2 BallPos {
			get{ return ball.BallPos;}
		}
		
		public Row[] MapPieces {
			get{ return mapPieces; }
			set { mapPieces = value;}
		}
		
		public void InitBall ()
		{
			ball = new BallModel (new IntVector2 (ballStartPos), ballStartDir);
		}
		
		public void ResetMap ()
		{
			mapState = MapModel.MapState.None;
			foreach (SpecialPartModel specialPart in SpecialParts) {
				specialPart.Reset ();
			}
			InitBall ();
		}

		/// <summary>
		/// Moves the ball in a direction.
		/// </summary>
		/// <param name="direction">Direction.</param>
		public bool MoveBall ()
		{
			if (ball.Move ()) {
				mapPieces [BallPos.y] [BallPos.x].MoveOn (ball);
				return true;
			} else if (ball.ballState == BallModel.BallState.Won) {
					mapState = MapState.Won;
				} else if (ball.ballState == BallModel.BallState.Dead) {
						mapState = MapState.Lost;
					}
			return false;
		}
		
		public IntVector2 SpecialPartExists (SpecialPartModel specialPart)
		{
			for (int row = 0; row < MapPieces.Length; row++) {
				for (int col = 0; col < MapPieces[row].Length; col++) {
					if (specialPart == MapPieces [row] [col].SpecialPart) {
						IntVector2 toReturn = new IntVector2 (col, row);
						return toReturn;
					}
				}
			}
			return null;
		}
		
		public bool PlaceSpecialPart (IntVector2 pos, SpecialPartModel specialPart)
		{
			IntVector2 specialPartPos = SpecialPartExists (specialPart);
			if (specialPartPos != null) {
				mapPieces [specialPartPos.y] [specialPartPos.x].RemoveSpecialPart ();
			}
			if (mapPieces [pos.y] [pos.x].SpecialPart == null && pos != ballStartPos) {
				mapPieces [pos.y] [pos.x].PlaceSpecialPart (specialPart);
				return true;
			}
			return false;
		}
		
		public IntVector2 FindMapPiece (MapPieceModel mapPiece)
		{
			for (int row = 0; row < MapPieces.Length; row++) {
				for (int col = 0; col < MapPieces[row].Length; col++) {
					if (mapPiece == MapPieces [row] [col]) {
						IntVector2 toReturn = new IntVector2 (col, row);
						return toReturn;
					}
				}
			}
			return null;
		}
	}

	[System.Serializable]
	public class Row
	{
		[SerializeField]
		private MapPieceModel[]
			Col = null;

		public MapPieceModel this [int index] {
			get {
				return Col [index];
			}
			set {
				Col [index] = value;
			}
		}

		public int Length {
			get {
				return Col.Length;
			}
		}
	}
}