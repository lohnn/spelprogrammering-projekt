﻿using UnityEngine;
using System.Collections;
using Math;
using Controller;

namespace Model.Map
{
	public class MapPieceModel : MonoBehaviour
	{
		[SerializeField]
		private string
			mapPiece = null;
		[SerializeField]
		private SpecialPartModel
			specialPart = null;
		public MapPieceController mapPieceController;
		public bool isDeadly = false;
		
		public void PlaceSpecialPart (SpecialPartModel specialPart)
		{
			this.specialPart = specialPart;
		}
		
		public SpecialPartModel SpecialPart{ get { return specialPart; } }
		
		public void RemoveSpecialPart ()
		{
			specialPart = null;
		}
		
		public string Name {
			get{ return mapPiece;}
		}

		public void MoveOn (BallModel ball)
		{
			if (isDeadly) {
				ball.ballState = BallModel.BallState.Dead;
			}
			if (specialPart != null) {
				specialPart.Collide (ball);
			}
		}
		
		public MapPieceModel Place (Vector3 pos, Transform parent, MapPieceController mapPieceController)
		{
			this.mapPieceController = mapPieceController;
			MapPieceModel toReturn = (MapPieceModel)Instantiate (this, pos, transform.rotation);
			toReturn.mapPieceController = mapPieceController;
			return toReturn;
		}
		
		public MapPieceModel Place (Vector2 pos, Transform parent, MapPieceController mapPieceController)
		{
			return Place (new Vector3 (pos.x, 0, pos.y), parent, mapPieceController);
		}
		
		public MapPieceModel Place (int x, int y, Transform parent, MapPieceController mapPieceController)
		{
			return Place (new Vector2 (x, y), parent, mapPieceController);
		}

		void OnMouseOver ()
		{
			mapPieceController.HoverOverMapPiece (this);
		}
		
		void OnMouseDown ()
		{
			mapPieceController.ClickOnMapPiece (this);
		}
	}
}