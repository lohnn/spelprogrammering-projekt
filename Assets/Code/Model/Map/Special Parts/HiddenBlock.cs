﻿using UnityEngine;
using System.Collections;

namespace Model.Map.SpecialParts
{
	public class HiddenBlock : SpecialPartModel
	{
		[SerializeField]
		public GameObject
			WalkableGroundObject = null;
		public bool isHidden = true;
		
		public void RevealGroundObject ()
		{
			//TODO: hardcoded for level 3!
			Instantiate (WalkableGroundObject, View.Camera.ConvertToWorldCoordinates (2, 2), new Quaternion ());
			this.gameObject.SetActive (false);
			isHidden = false;
		}
		
		public override void Collide (BallModel ball)
		{
			if (isHidden) {
				ball.ballState = BallModel.BallState.Dead;
			}
		}
		
		public override void Reset ()
		{
		}
	}
}