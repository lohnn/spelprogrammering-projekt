using UnityEngine;
using System.Collections;
using Math;

namespace Model.Map.SpecialParts
{
	public class Arrow : SpecialPartModel
	{
		public IntVector2.Direction direction;
		/// <summary>
		/// How many times the ball has to pass it before it activates.
		/// </summary>
		[SerializeField]
		private int
			activateAfter = 0;
		/// <summary>
		/// How many times the ball can pass before it dies/stop working, -1 is infinite.
		/// </summary>
		[SerializeField]
		private int
			lifeSpan = -1;
		private int timesPassed = 0;

		public override void Collide (BallModel ball)
		{
			if (lifeSpan < 0 && timesPassed >= activateAfter) {
				ball.Direction = direction;
				
			} else
				timesPassed++;
		}
		
		public override void Reset ()
		{
			timesPassed = 0;
		}
		
		public override string ToString ()
		{
			return string.Format ("[Arrow] : {0}", direction);
		}
	}
}