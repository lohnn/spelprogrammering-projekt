﻿using UnityEngine;
using System.Collections;

namespace Model.Map.SpecialParts
{
	public class Button : SpecialPartModel
	{
		[SerializeField]
		private HiddenBlock
			blockToReveal = null;

		public override void Collide (BallModel ball)
		{
			if (blockToReveal != null)
				blockToReveal.RevealGroundObject ();
		}
		
		public override void Reset ()
		{
		}
	}
}