﻿using UnityEngine;
using System.Collections;

namespace Model.Map.SpecialParts
{
	public class Goal : SpecialPartModel
	{
		public override void Collide (BallModel ball)
		{
			ball.ballState = BallModel.BallState.Won;
		}

		public override void Reset ()
		{
		}
	}
}