﻿using UnityEngine;
using System.Collections;
using Math;

namespace Model.Map
{
	public class BallModel
	{
		public enum BallState
		{
			None,
			Won,
			Dead
		}
		private IntVector2
			ballPos;
		private IntVector2.Direction
			direction;
		public BallState ballState = BallState.None;
		
		public BallModel (IntVector2 ballPos, IntVector2.Direction direction)
		{
			this.ballPos = ballPos;
			this.direction = direction;
		}
		
		public bool Move ()
		{
			if (ballState != BallState.Won && ballState != BallState.Dead) {
				ballPos.Move (direction);
				return true;
			}
			return false;
		}
	
		public IntVector2 BallPos {
			get { return ballPos;}
		}
		
		public IntVector2.Direction Direction {
			get { return direction;}
			set { direction = value;}
		}
	}
}