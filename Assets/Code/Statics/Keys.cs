﻿using UnityEngine;
using System.Collections;

namespace Statics
{
	public class Keys
	{
		public static string KEY_JUMP = "Jump",
			KEY_CANCEL = "Fire2",
			KEY_PAUSE = "Pause";
	}
}