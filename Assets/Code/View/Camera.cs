﻿using UnityEngine;
using System.Collections;
using Math;

namespace View
{
	public class Camera
	{
		public static Vector3 ConvertToWorldCoordinates (int x, float y, int z)
		{
			return new Vector3 (x, y, -z);
		}
	
		public static Vector3 ConvertToWorldCoordinates (int x, int y)
		{
			return ConvertToWorldCoordinates (x, 0, y);
		}
	
		public static Vector3 ConvertToWorldCoordinates (IntVector2 pos, float y)
		{
			return ConvertToWorldCoordinates (pos.x, y, pos.y);
		}
	
		public static Vector3 ConvertToWorldCoordinates (IntVector2 pos)
		{
			return ConvertToWorldCoordinates (pos, 0);
		}
	}
}