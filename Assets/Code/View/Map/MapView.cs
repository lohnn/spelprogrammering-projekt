using UnityEngine;
using System.Collections;
using Model.Map;
using Model.Map.SpecialParts;
using Math;
using Statics;
using View.GUI;
using System.Collections.Generic;

namespace View.Map
{
	public class MapView : MonoBehaviour
	{
		[SerializeField]
		private List<MapModel>
			map = new List<MapModel> ();
		[SerializeField]
		private MapModel
			currentMapModel = null;
		[SerializeField]
		private int
			currentMap = 0;
		[SerializeField]
		private GameObject
			ball = null;
		private BallView ballView;
		private bool isRunning;
		[SerializeField]
		private View.GUI.GUI
			gui = null;
		Controller.MapPieceController mapPieceController;
		SpecialPartModel placingSpecialPartModel = null;
		GameObject mapPiecesParent;
		protected bool paused;
		[SerializeField]
		private AudioSource
			ballMoveSound = null;
		
		// Use this for initialization
		void Start ()
		{
			mapPieceController = new Controller.MapPieceController ();
			Controller.MapPieceController._hoverOverMapPiece += new HoverOverMapPieceHandler (HoverOverMapPiece);
			Controller.MapPieceController._clickOnMapPiece += new ClickOnMapPieceHandler (PlaceSpecialPart);

			GUI.GUI._guiFunction += new EventHandler (GuiButtonEventHappened);
			GUI.GUI._pickSpecialItemFunction += new PickSpecialItemHandler (StartPlacingSpecialPart);

			mapPiecesParent = new GameObject ("Map Pieces");
			InitMap ();
		}
		
		/// <summary>
		/// Reloads the tiles and stuff for the map.
		/// </summary>
		[ContextMenu("Init map")]
		private void InitMap ()
		{
			if (currentMapModel != null) {
				for (int row = 0; row < currentMapModel.MapPieces.Length; row++) {
					for (int col = 0; col < currentMapModel.MapPieces[row].Length; col++) {
						Destroy (currentMapModel.MapPieces [row] [col].gameObject);
					}
				}
				for (int i = 0; i< currentMapModel.SpecialParts.Length; i++) {
					Debug.Log ("removing specialparts?");
					Destroy (currentMapModel.SpecialParts [i].Model.gameObject);
					Destroy (currentMapModel.SpecialParts [i].gameObject);
				}
				
				Destroy (currentMapModel.gameObject);
			}
			currentMapModel = (MapModel)Instantiate (map [currentMap]);
			for (int row = 0; row < currentMapModel.MapPieces.Length; row++) {
				for (int col = 0; col < currentMapModel.MapPieces[row].Length; col++) {
					currentMapModel.MapPieces [row] [col] = currentMapModel.MapPieces [row] [col].Place (
						Camera.ConvertToWorldCoordinates (col, row), mapPiecesParent.transform, mapPieceController);	
					currentMapModel.MapPieces [row] [col].transform.parent = mapPiecesParent.transform;
				}
			}
			//TODO: not too important, but makes one copy of whole object when instantiate...
			for (int i = 0; i< currentMapModel.SpecialParts.Length; i++) {
				currentMapModel.SpecialParts [i] = (SpecialPartModel)Instantiate (currentMapModel.SpecialParts [i]);
			}
			gui.PlaceSpecialItems (currentMapModel.SpecialParts);
			ResetMap ();
		}
		/// <summary>
		/// Resets (initiates) the map.
		/// </summary>
		private void ResetMap ()
		{
			currentMapModel.ResetMap ();
			isRunning = false;
			gui.HideAllScreens ();
			ballView = new BallView (currentMapModel.ball, ball, ballMoveSound);
		}
		
		/// <summary>
		/// Called when hovered the over a map piece.
		/// </summary>
		/// <param name="hoveredObject">Hovered object.</param>
		public void HoverOverMapPiece (MapPieceModel hoveredObject)
		{
			if (placingSpecialPartModel != null) {
				PlaceSpecialPart (placingSpecialPartModel, currentMapModel.FindMapPiece (hoveredObject));
			}
		}
		
		void StartPlacingSpecialPart (int id)
		{
			placingSpecialPartModel = currentMapModel.SpecialParts [id];
		}
		
		void PlaceSpecialPart (MapPieceModel clickedObject)
		{
			if (placingSpecialPartModel != null) {
				PlaceSpecialPart (placingSpecialPartModel, currentMapModel.FindMapPiece (clickedObject));
				placingSpecialPartModel = null;
			}
		}
		
		void StopPlacingSpecialPart ()
		{
			if (instantiatedSpecialParts.Contains (placingSpecialPartModel)) {
				Debug.Log ("destroying");
				Destroy (placingSpecialPartModel.Model);
				instantiatedSpecialParts.Remove (placingSpecialPartModel);
			}
			placingSpecialPartModel = null;
		}
		
		void GuiButtonEventHappened (GUI.GUI.GUIFunctions function)
		{
			switch (function) {
				case View.GUI.GUI.GUIFunctions.GoBall:
					isRunning = true;
					break;
				case View.GUI.GUI.GUIFunctions.PlayAgain:
					ResetMap ();
					break;
				case View.GUI.GUI.GUIFunctions.MainMenu:
					Application.LoadLevel (Statics.Levels.MainMenu.ToString ());
					break;
				case View.GUI.GUI.GUIFunctions.NextMap:
					if (currentMap + 1 < map.Count) {
						currentMap++;
						InitMap ();
					} else {
						gui.HideAllScreens ();
						gui.ShowNoMoreMapsScreen ();
					}
					break;
				case View.GUI.GUI.GUIFunctions.Unpause:
					Pause (false);
					break;
			}
		}
	
		void MoveBall ()
		{
			if (currentMapModel.MoveBall ()) {
				ballView.Move ();
			}
			if (currentMapModel.mapState == MapModel.MapState.Won) {
				ShowVictory ();
			}
			if (currentMapModel.mapState == MapModel.MapState.Lost) {
				ShowLost ();
			}
		}
		
		void ShowVictory ()
		{
			gui.ShowVictoryScreen ();
		}
		
		void ShowLost ()
		{
			gui.ShowLostScreen ();
		}
		
		List<SpecialPartModel> instantiatedSpecialParts = new List<SpecialPartModel> ();
		
		void PlaceSpecialPart (SpecialPartModel specialPart, IntVector2 pos)
		{
			if (currentMapModel.PlaceSpecialPart (pos, specialPart)) {
				float rotationMultiplier = 1;
				if (specialPart is Arrow) {
					Arrow arrow = (Arrow)specialPart;
					rotationMultiplier = (int)arrow.direction;
				}
			
				if (instantiatedSpecialParts.Contains (specialPart)) {
					specialPart.Model.transform.position = Camera.ConvertToWorldCoordinates (pos, 0.5f);
				} else {
					specialPart.Model = (GameObject)Instantiate (specialPart.Model,
				             Camera.ConvertToWorldCoordinates (pos, 0.5f)
				             , specialPart.Model.transform.rotation 
						* Quaternion.Euler (0, 0, 90 * rotationMultiplier));
					instantiatedSpecialParts.Add (specialPart);
				}
			}
		}
		
		void Pause (bool pause)
		{
			if (currentMapModel.mapState != MapModel.MapState.Won 
				&& currentMapModel.mapState != MapModel.MapState.Lost) {
				if (pause) {
					Time.timeScale = 0;
					paused = true;
					gui.ShowPauseScreen ();
				} else {
					Time.timeScale = 1;
					paused = false;
					gui.HideAllScreens ();
				}
			}
		}
	
		// Update is called once per frame
		void Update ()
		{
			if (Input.GetButtonDown (Keys.KEY_PAUSE)) {
				Pause (!paused);
			}
			if (Input.GetButtonDown (Keys.KEY_JUMP)) {
				PlaceSpecialPart ((Arrow)currentMapModel.SpecialParts [0],
				           new IntVector2 (2, 4));
				PlaceSpecialPart ((Arrow)currentMapModel.SpecialParts [1],
				            new IntVector2 (2, 1));
				PlaceSpecialPart ((Arrow)currentMapModel.SpecialParts [2],
				            new IntVector2 (1, 4));
			}
			if (Input.GetButtonDown (Keys.KEY_CANCEL)) {
				//TODO: fix cancellation of special part placement
				//StopPlacingSpecialPart();
			}
			if (currentMapModel.mapState != MapModel.MapState.Won || ballView.IsMoving) {
				if (isRunning && !ballView.IsMoving) {
					MoveBall ();
				}
				ballView.Update ();
			}
		}
	}
}