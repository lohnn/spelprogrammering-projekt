using UnityEngine;
using System.Collections;
using Math;
using Model.Map;

namespace View
{
	public class BallView
	{
		private BallModel ballModel;
		private GameObject ball;
		private bool isMoving = false;
		private float startTime;
		public Vector3 startPos;
		public Vector3 endPos;
		private Quaternion startRot;
		private Quaternion endRot;
		public float timeForMovement = 1;
		AudioSource moveSound;
		
		public bool IsMoving {
			get { return isMoving;}
		}
		
		public BallView (BallModel ballModel, GameObject ball, AudioSource moveSound)
		{
			this.moveSound = moveSound;
			this.ballModel = ballModel;
			this.ball = ball;
			ball.transform.position = Camera.ConvertToWorldCoordinates (ballModel.BallPos, 1);
		}

		public void Move ()
		{
			startTime = Time.time;
			isMoving = true;
			startPos = ball.transform.position;
			endPos = Camera.ConvertToWorldCoordinates (ballModel.BallPos, 1);
			startRot = ball.transform.rotation;
			
			endRot = new Quaternion (startRot.x, startRot.y, startRot.z, startRot.w);
			Vector3 temp = endPos - startPos;
			temp = new Vector3 (temp.z, 0, -temp.x);
			temp *= 1 / Mathf.PI * 360;
			endRot = Quaternion.Euler (temp) * endRot;
			moveSound.Play ();
			//endRot *= Quaternion.Euler (temp);
			//Debug.Log (string.Format ("{0} : {1}", startRot, endRot));
		}
		
		// Update is called once per frame
		public void Update ()
		{
			if (isMoving) {
				float fracJourney = (Time.time - startTime) / timeForMovement;
				ball.transform.position = Vector3.Lerp (startPos, endPos, fracJourney);
				ball.transform.rotation = Quaternion.Lerp (startRot, endRot, fracJourney);
				if (fracJourney >= 1)
					isMoving = false;
			}
		}
	}
}
