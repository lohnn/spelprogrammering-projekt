﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Model.Map;
using Model.Map.SpecialParts;

namespace View.GUI
{
	[System.Serializable]
	public class SpecialPartsGUI
	{
		[SerializeField]
		private GameObject
			background = null;
		[SerializeField]
		private GUISpecialPartsButtonClick
			arrowImage = null;
		List<GUISpecialPartsButtonClick> currentSpecialParts = new List<GUISpecialPartsButtonClick> ();
	
		public void PlaceSpecialParts (SpecialPartModel[] specialParts, View.GUI.GUI gui)
		{
			foreach (GUISpecialPartsButtonClick go in currentSpecialParts) {
				GameObject.Destroy (go.gameObject);
			}
			currentSpecialParts.Clear ();
		
			Vector3 pos = background.transform.localPosition;
			pos.x = specialParts.Length * 50;
			background.transform.localPosition = pos;
			Vector3 scale = background.transform.localScale;
			scale.x = specialParts.Length * 100;
			background.transform.localScale = scale;
		
			for (int i = 0; i < specialParts.Length; i++) {
				if (specialParts [i] is Arrow) {
					GUISpecialPartsButtonClick temp = (GUISpecialPartsButtonClick)GameObject.Instantiate (arrowImage);
					temp.transform.parent = background.transform.parent;
					temp.transform.localPosition = new Vector3 (50 + 100 * i, -50, 45);
					temp.transform.localScale = new Vector3 (100, 100, 10);
					Arrow tempArrow = (Arrow)specialParts [i];
					temp.transform.rotation *= Quaternion.Euler (0, 0, -90 * (int)tempArrow.direction);
					temp.Init (gui, i);
					currentSpecialParts.Add (temp);
				}
			}
		}
	}
}
