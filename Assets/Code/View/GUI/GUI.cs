﻿using UnityEngine;
using System.Collections;
using Model.Map;

public delegate void EventHandler (View.GUI.GUI.GUIFunctions guiFunction);

public delegate void PickSpecialItemHandler (int id);

namespace View.GUI
{
	public class GUI : MonoBehaviour
	{
		public enum GUIFunctions
		{
			NoFunction,
			GoBall,
			NextMap,
			PlayAgain,
			MainMenu,
			Quit,
			Pause,
			Unpause,
			ShowMapsScreen
		}
		[SerializeField]
		private GameObject
			victoryScreen = null;
		[SerializeField]
		private GameObject
			looseScreen = null;
		[SerializeField]
		private GameObject
			lastMapWonScreen = null;
		[SerializeField]
		private GameObject
			pauseScreen = null;
		[SerializeField]
		private SpecialPartsGUI
			specialItemsGUI = new SpecialPartsGUI ();
		
		public static event EventHandler _guiFunction;
		public static event PickSpecialItemHandler _pickSpecialItemFunction;
		
		public void PlaceSpecialItems (SpecialPartModel[] specialParts)
		{
			specialItemsGUI.PlaceSpecialParts (specialParts, this);
		}
		
		public void HideAllScreens ()
		{
			victoryScreen.gameObject.SetActive (false);
			looseScreen.gameObject.SetActive (false);
			lastMapWonScreen.gameObject.SetActive (false);
			pauseScreen.gameObject.SetActive (false);
		}
		
		public bool IsShowingScreen ()
		{
			if (victoryScreen.gameObject.activeSelf ||
				looseScreen.gameObject.activeSelf ||
				lastMapWonScreen.gameObject.activeSelf ||
				pauseScreen.gameObject.activeSelf) {
				return true;
			}
			return false;
		}

		public void ShowPauseScreen ()
		{
			pauseScreen.SetActive (true);
		}

		public void ShowVictoryScreen ()
		{
			victoryScreen.SetActive (true);
		}
		
		public void ShowNoMoreMapsScreen ()
		{
			lastMapWonScreen.SetActive (true);
		}
		
		public void ShowLostScreen ()
		{
			looseScreen.SetActive (true);
		}
		
		public void IngameGuiButtonClick (GUIFunctions guiFunction)
		{
			_guiFunction.Invoke (guiFunction);
		}
		
		public void PickSpecialItemButtonClick (int id)
		{
			_pickSpecialItemFunction.Invoke (id);
		}
	}
}