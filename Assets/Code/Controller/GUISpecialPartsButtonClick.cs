﻿using UnityEngine;
using System.Collections;

namespace View.GUI
{
	public class GUISpecialPartsButtonClick : MonoBehaviour
	{
		[SerializeField]
		private View.GUI.GUI
			gui = null;
		[SerializeField]
		private int
			itemId = 0;
		
		public void Init (View.GUI.GUI gui, int id)
		{
			this.gui = gui;
			this.itemId = id;
		}
		
		void OnMouseDown ()
		{
			gui.PickSpecialItemButtonClick (itemId);
		}
	}
}
