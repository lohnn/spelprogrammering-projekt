﻿using UnityEngine;
using System.Collections;

namespace Controller
{
	public class MainMenuController : MonoBehaviour
	{
		// Use this for initialization
		void Start ()
		{
			View.GUI.GUI._guiFunction += new EventHandler (GuiButtonEventHappened);
		}

		void GuiButtonEventHappened (View.GUI.GUI.GUIFunctions function)
		{
			if (function == View.GUI.GUI.GUIFunctions.ShowMapsScreen) {
				Application.LoadLevel(Statics.Levels.Game.ToString());
			} else if (function == View.GUI.GUI.GUIFunctions.Quit) {
					Application.Quit ();
				}
		}
	}
}