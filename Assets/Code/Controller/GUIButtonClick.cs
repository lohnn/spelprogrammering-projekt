﻿using UnityEngine;
using System.Collections;

namespace Controller
{
	public class GUIButtonClick : MonoBehaviour
	{
		[SerializeField]
		private View.GUI.GUI
			gui = null;
		[SerializeField]
		private View.GUI.GUI.GUIFunctions
			runFunction = View.GUI.GUI.GUIFunctions.NoFunction;

		void OnMouseDown ()
		{
			gui.IngameGuiButtonClick (runFunction);
		}
	}
}