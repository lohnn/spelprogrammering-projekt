﻿using UnityEngine;
using System.Collections;
using Model.Map;

public delegate void HoverOverMapPieceHandler (MapPieceModel hoveredObject);
public delegate void ClickOnMapPieceHandler (MapPieceModel hoveredObject);
namespace Controller
{
	public class MapPieceController
	{
		public static event HoverOverMapPieceHandler _hoverOverMapPiece;
		public static event ClickOnMapPieceHandler _clickOnMapPiece;
		/// <summary>
		/// Call when hovered over a map piece.
		/// </summary>
		/// <param name="hoveredObject">Hovered object.</param>
		public void HoverOverMapPiece (MapPieceModel hoveredObject)
		{
			_hoverOverMapPiece.Invoke (hoveredObject);
		}
		
		/// <summary>
		/// Call when clicked a map piece.
		/// </summary>
		/// <param name="hoveredObject">Clicked object.</param>
		public void ClickOnMapPiece (MapPieceModel clickedObject)
		{
			_clickOnMapPiece.Invoke (clickedObject);
		}
	}
}
